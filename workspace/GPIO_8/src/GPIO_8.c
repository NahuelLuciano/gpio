/*
===============================================================================
 Name        : GPIO_8.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : Mostrar en un displays de 7 segmentos, numeros aleatorios a intervalos de 5 seg
===============================================================================
*/

#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

/////////////////LIBRERIAS UTILIZADAS//////////////////////////
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
//#include "stdlib.h"
///////////////VARIABLES GLOBALES/////////////////////////////
xQueueHandle Cola;
xSemaphoreHandle Semaforo;
uint32_t display[10]={0x7E,0x30,0x6D,0x79,0x33,0x5B,0x5F,0x70,0x7F,0x7B}; //CATODO COMUN
/////////////////DISPOSICION DE PINES EN EL STICK///////////////////////////////
//		P2[0] P2[1] P[2] P2[3] P2[4] P2[5] P2[6]  ------> PUERTOX[PIN]		//
//		 42		43	 44	   45   46    47    48	  ------> PINES DEL STICK	//
//		 G      F    E     D     C     B    A	  ------> PINES DEL DISPLAY //
////////////////DEFINICIONES O MACROS/////////////////////////////////
#define	tiempo1	500/portTICK_RATE_MS
#define	tiempo2	2000/portTICK_RATE_MS
#define	tiempo3	1000/portTICK_RATE_MS
#define mascara ((1<<0)|(1<<1)|(1<<2)|(1<<3)|(1<<4)|(1<<5)|(1<<6))
////////////////////////TAREAS Y FUNCIONES/////////////////////
void static t_Productora()
{int i=0;
 int numero;

	while(1)
	{	i=0;

		xSemaphoreTake(Semaforo,portMAX_DELAY);
		for(i=0; i<10 ; i++)
		{
			numero=rand()%10;
			xQueueSend(Cola,&numero,0);
		}
		xSemaphoreGive(Semaforo);
	}

}

void static t_Receptora()
{int i=0,numero;
 uint32_t aux;
	while(1)
	{
		xSemaphoreTake(Semaforo,portMAX_DELAY);
		for(i=0; i<10; i++)
		{
			xQueueReceive(Cola,&numero,0);
			aux=display[numero];
			Chip_GPIO_SetMaskedPortValue(LPC_GPIO,2,aux);
			vTaskDelay(tiempo3);
		}
		xSemaphoreGive(Semaforo);
	}

}

//////////////////////////PROGRAMA PRINCIPAL///////////////////////////////////
int main (void)
{
	Cola=xQueueCreate(10,sizeof(int));
	Semaforo=xSemaphoreCreateMutex();
/*******************SE CONFIGURA EL PUERTO 2 COMO SALIDA*******************/
	Chip_GPIO_SetPortDIROutput(LPC_GPIO,2,mascara);
	Chip_GPIO_SetPortMask(LPC_GPIO,2,~mascara);
/*************************************************************************/
	SystemCoreClockUpdate();
	Board_Init();

	xTaskCreate(t_Receptora,"t_Receptora",1024,0,1,0);
	xTaskCreate(t_Productora,"t_Productora",1024,0,1,0);

	vTaskStartScheduler();
	while(1);
}


