/*
===============================================================================
 Name        : GPIO_9.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : Crear un dado electronico, el cual mostrara en un display de 7 segmentos
 	 	 	   los numeros de 0 a 6 rapidamente. Mediante un pulsador se tomara un numero al azar
 	 	 	   el cual representa la cara del dado.
 NOTA		 : Se puede reemplazar rand() por un "for(i=1;i<7;i++)" para evitar que se repita el mismo numero varias veces.
===============================================================================
*/

#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#define tiempo 100/portTICK_RATE_MS
#define mascara ((1<<0)|(1<<1)|(1<<2)|(1<<3)|(1<<4)|(1<<5)|(1<<6))

xTaskHandle imprimir;/*se utilizan como referencia para que otras apis accedan a las tareas*/
xTaskHandle	azar;
uint32_t display[10]={0x7E,0x30,0x6D,0x79,0x33,0x5B,0x5F,0x70,0x7F,0x7B};
volatile uint32_t aux;

static void t_imprimir (void)
{
	while(1)
	{

		if(Chip_GPIO_GetPinState(LPC_GPIO,0,28)==FALSE)
		{
			Chip_GPIO_SetMaskedPortValue(LPC_GPIO,2,aux);
		}
		else
		{
			vTaskSuspend(NULL);
		}

	}
}

static void t_azar (void)
{int i;
	while(1)
		{//i=1;
			while(Chip_GPIO_GetPinState(LPC_GPIO,0,28))
			{
				i=srand()%6+1;
				aux=display[i];
				Chip_GPIO_SetMaskedPortValue(LPC_GPIO,2,aux);
				vTaskDelay(tiempo);
				//i++;
				//if(i==7)
					//i=1;
			}
			vTaskResume(imprimir);

		}
}

int main (void)
{
	SystemCoreClockUpdate();
	Board_Init();

	Chip_GPIO_SetPortDIROutput(LPC_GPIO,2,mascara);
	Chip_GPIO_SetPortMask(LPC_GPIO,2,~mascara);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO,0,28);


	xTaskCreate(t_imprimir,"t_imprimir",1024,0,3,&imprimir);
	xTaskCreate(t_azar,"t_azar",1024,0,1,&azar);
	vTaskSuspend(imprimir);

	vTaskStartScheduler();

	while(1);
}
