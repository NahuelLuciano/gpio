/*
===============================================================================
 Name        : GPIO_4.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : Al mantener presionado el pulsador, el led2 (del stick) comienza a parpadear.
 	 	 	   En caso contrario estara apagado.
===============================================================================
*/

#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

xSemaphoreHandle xSemaforo; // Tiene que ser Global, porque lo tiene que poder ver todas las tareas

int pulsador;

#define tiempo1	500/portTICK_RATE_MS //Tiempo de parpadeo


static void taskLEDoff (void)
{
	while(1)
	{
	xSemaphoreTake(xSemaforo,portMAX_DELAY);
	Chip_GPIO_WritePortBit(LPC_GPIO,0,22,0);
	xSemaphoreGive(xSemaforo);
	}
}

static void taskLEDon (void)
{
	while(1)
		{
		xSemaphoreTake(xSemaforo,portMAX_DELAY);
		pulsador=Chip_GPIO_ReadPortBit(LPC_GPIO,2,12);
		while(pulsador==0)
		{
			Chip_GPIO_WritePortBit(LPC_GPIO,0,22,1);
			vTaskDelay(tiempo1);
			Chip_GPIO_WritePortBit(LPC_GPIO,0,22,0);
			vTaskDelay(tiempo1);
			pulsador=Chip_GPIO_ReadPortBit(LPC_GPIO,2,12);
		}
		xSemaphoreGive(xSemaforo);
		}
}

int main (void)
{
	SystemCoreClockUpdate();
	Board_Init();
	xTaskCreate(taskLEDoff,"taskLEDoff",1024,0,1,0);
	xTaskCreate(taskLEDon,"taskLEDon",1024,0,1,0);
	xSemaforo = xSemaphoreCreateMutex();

	vTaskStartScheduler();

	while(1);
}
