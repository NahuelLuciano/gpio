/*
===============================================================================
 Name        : GPIO_5.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : Se contaran la cantidad de veces que parpadea el led del pin 22 (del stick),
 	 	 	   luego esa misma cantidad de veces se vera reflejada en otro led (pin 27).
===============================================================================
*/

#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>
//*************LIBRERIAS********************//
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
//***********VARIABLES********************//
xSemaphoreHandle xSemaforo;
xQueueHandle	 xCola;
int pulsador;
//**********DEFINICIONES*****************//
#define tiempo1	500/portTICK_RATE_MS //Tiempo de parpadeo
#define rebote	50/portTICK_RATE_MS
//**********DEFINICION DE TAREAS (SUS FUNCIONES)********//
static void t_repetir (void)
{int cant=0;
	while(1)
	{
	xSemaphoreTake(xSemaforo,portMAX_DELAY);
	Chip_GPIO_WritePortBit(LPC_GPIO,2,13,0);
	xQueueReceive(xCola,&cant,0);
	while(cant!=0)
	{
		Chip_GPIO_WritePortBit(LPC_GPIO,2,13,1);
		vTaskDelay(tiempo1);
		Chip_GPIO_WritePortBit(LPC_GPIO,2,13,0);
		vTaskDelay(tiempo1);
		cant--;
	}
	xSemaphoreGive(xSemaforo);
	}
}

static void t_parpadear (void)
{int cant;
	while(1)
		{
		xSemaphoreTake(xSemaforo,portMAX_DELAY);
		pulsador=Chip_GPIO_GetPinState(LPC_GPIO,2,12);
		vTaskDelay(rebote);
		cant=0;
		while(pulsador==0)
		{
			Chip_GPIO_WritePortBit(LPC_GPIO,0,22,1);
			vTaskDelay(tiempo1);
			Chip_GPIO_WritePortBit(LPC_GPIO,0,22,0);
			vTaskDelay(tiempo1);
			cant++;
			pulsador=Chip_GPIO_GetPinState(LPC_GPIO,2,12);
		}
		xQueueSendToBack(xCola,&cant,0);
		xSemaphoreGive(xSemaforo);
		}
}
//****************PROGRAMA PRINCIPAL************************//
int main (void)
{
	SystemCoreClockUpdate();
	Board_Init();
///////////CONFIGURACION DE PIN COMO SALIDA/////////////////
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,2,13);
//////////CREACION DE TAREAS, COLAS Y SEMAFOROS////////////
	xTaskCreate(t_repetir,"t_repetir",1024,0,1,0);
	xTaskCreate(t_parpadear,"t_parpadear",1024,0,1,0);
	xSemaforo = xSemaphoreCreateMutex();
	xCola=xQueueCreate(4,sizeof(int));
/////////INICIO DELSCHEDULER//////////////////////////////
	vTaskStartScheduler();

	while(1);
}
