/*
===============================================================================
 Name        : GPIO_7.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : Carga una cola con numeros de un digito, para luego encender 4 leds al mismo
 	 	 	   tiempo. Se deben configurar los pines nesesarios del puerto 2 como salida.
===============================================================================
*/

#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

/////////////////LIBRERIAS UTILIZADAS//////////////////////////
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
///////////////VARIABLES GLOBALES/////////////////////////////
xQueueHandle Cola;
xSemaphoreHandle Semaforo;
uint32_t display[10]={0x3F,0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x67};
////////////////DEFINICIONES O MACROS/////////////////////////////////
#define	tiempo1	500/portTICK_RATE_MS
#define	tiempo2	2000/portTICK_RATE_MS
#define	tiempo3	5000/portTICK_RATE_MS
#define mascara ((1<<6)|(1<<7)|(1<<8)|(1<<9)|(1<<10)|(1<<11)|(1<<12))
////////////////////////TAREAS Y FUNCIONES/////////////////////
void static t_Productora()
{int i=0, numero=0;

	while(1)
	{	numero=0;

		xSemaphoreTake(Semaforo,portMAX_DELAY);
		for(i=0; i<4 ; i++)
		{
			numero=i+1;
			xQueueSend(Cola,&numero,0);
		}
		xSemaphoreGive(Semaforo);
	}

}

void static t_Receptora()
{int i=0, numero=0;
	while(1)
	{
		xSemaphoreTake(Semaforo,portMAX_DELAY);
		for(i=0; i<4; i++)
		{
			xQueueReceive(Cola,&numero,0);
			while(numero>0 && numero!=0)
			{
				Chip_GPIO_WritePortBit(LPC_GPIO,0,22,1);
				vTaskDelay(tiempo1);
				Chip_GPIO_WritePortBit(LPC_GPIO,0,22,0);
				vTaskDelay(tiempo1);
				numero--;
			}
			vTaskDelay(tiempo2);
		}
		Chip_GPIO_SetMaskedPortValue(LPC_GPIO,2,0x1FC0);
		vTaskDelay(tiempo3);
		Chip_GPIO_SetMaskedPortValue(LPC_GPIO,2,0x00);
		xSemaphoreGive(Semaforo);
	}

}
//////////////////////////PROGRAMA PRINCIPAL///////////////////////////////////
int main (void)
{
	Cola=xQueueCreate(4,sizeof(int));
	Semaforo=xSemaphoreCreateMutex();
/*******************SE CONFIGURA EL PUERTO 2 COMO SALIDA*******************/
	Chip_GPIO_SetPortDIROutput(LPC_GPIO,2,mascara);
	Chip_GPIO_SetPortMask(LPC_GPIO,2,~mascara);

	SystemCoreClockUpdate();
	Board_Init();

	xTaskCreate(t_Productora,"t_Productora",1024,0,1,0);
	xTaskCreate(t_Receptora,"t_Receptora",1024,0,1,0);

	vTaskStartScheduler();

	while(1);
}


