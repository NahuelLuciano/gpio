/*
===============================================================================
 Name        : gpio_2.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : Se utilizara un puslador para encender el led. Al mantener presionado el
 	 	 	   el pulsador el led permanece encendido. Se enciende cuando el pulsador esta a nivel
 	 	 	   bajo (0).
 NOTA		 : La hoja de datos del LPC1769 indica que cuando este arranca o es reiniciado los pines
 	 	 	   estan configurados con resistencia pull-up (estan a nivel alto). Ademas se hara uso del
 	 	 	   led 2 (del stick).
===============================================================================
*/

#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>
//*************************LIBRERIAS UTILIZADAS********************//
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
//*************************VARIABLES GLOBALES******************************//
xSemaphoreHandle xSemaforo; // Tiene que ser Global, porque lo tiene que poder ver todas las tareas
int pulsador;
//***********************DEFINICION DE TAREAS (FUNCIONES)********************//
static taskLEDoff (void)
{
	while(1)
	{
	xSemaphoreTake(xSemaforo,portMAX_DELAY);
	if(pulsador!=0)
	{
		Chip_GPIO_WritePortBit(LPC_GPIO,0,22,0);
	}
	xSemaphoreGive(xSemaforo);
	}
}

static taskLEDon (void)
{
	while(1)
		{
		xSemaphoreTake(xSemaforo,portMAX_DELAY);
		pulsador=Chip_GPIO_ReadPortBit(LPC_GPIO,2,12);
		while(pulsador==0)
		{
			Chip_GPIO_WritePortBit(LPC_GPIO,0,22,1);
			pulsador=Chip_GPIO_ReadPortBit(LPC_GPIO,2,12);
		}
		xSemaphoreGive(xSemaforo);
		}
}
//************************PROGRAMA PRINCIPAL***********************//
int main (void)
{
	SystemCoreClockUpdate();
	Board_Init();
	xTaskCreate(taskLEDoff,"taskLEDoff",1024,0,1,0);
	xTaskCreate(taskLEDon,"taskLEDon",1024,0,1,0);
	xSemaforo = xSemaphoreCreateMutex();

	vTaskStartScheduler();

	while(1);
}
