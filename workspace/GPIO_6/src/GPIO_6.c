/*
===============================================================================
 Name        : GPIO_6.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : Se generan 4 numeros aleatorios que se almacenan en una cola, para luego mostrarse
 	 	 	   en un displays de 8 segmentos. Los numeros seran entre 0 y 9.
 NOTA:		 : Como alternativa al displays de 8 segmentos se puede usar el led 2 del stick para
 	 	 	   representar cada numero. El led2 titila la cantidad de veces indicada por el numero.
===============================================================================
*/

#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>
/////////////////LIBRERIAS UTILIZADAS//////////////////////////
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
///////////////VARIABLES GLOBALES/////////////////////////////
xQueueHandle Cola;
xSemaphoreHandle Semaforo;
////////////////DEFINICIONES/////////////////////////////////
#define	tiempo1	500/portTICK_RATE_MS
#define	tiempo2	2000/portTICK_RATE_MS
#define	tiempo3	5000/portTICK_RATE_MS
////////////////////////TAREAS Y FUNCIONES/////////////////////
void static t_Productora (void)
{int i=0, numero=0;

	while(1)
	{	numero=0;

		xSemaphoreTake(Semaforo,portMAX_DELAY);
		for(i=0; i<4 ; i++)
		{
			numero=i+1;
			xQueueSendToBack(Cola,&numero,0);
		}
		xSemaphoreGive(Semaforo);
	}

}

void static t_Receptora(void)
{int i=0, numero=0;
	while(1)
	{
		xSemaphoreTake(Semaforo,portMAX_DELAY);
		for(i=0; i<4; i++)
		{
			xQueueReceive(Cola,&numero,0);
			while(numero>0 && numero!=0)
			{
				Chip_GPIO_WritePortBit(LPC_GPIO,0,22,1);
				vTaskDelay(tiempo1);
				Chip_GPIO_WritePortBit(LPC_GPIO,0,22,0);
				vTaskDelay(tiempo1);
				numero--;
			}
			vTaskDelay(tiempo2);
		}
		Chip_GPIO_SetPinState(LPC_GPIO, 2, 13, 1);
		vTaskDelay(tiempo3);
		Chip_GPIO_SetPinState(LPC_GPIO, 2, 13, 0);
		xSemaphoreGive(Semaforo);
	}

}
//////////////////////////PROGRAMA PRINCIPAL///////////////////////////////////
int main (void)
{
	Cola=xQueueCreate(4,sizeof(int));
	Semaforo=xSemaphoreCreateMutex();
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,2, 13);

	SystemCoreClockUpdate();
	Board_Init();

	xTaskCreate(t_Productora,"t_Productora",1024,0,1,0);
	xTaskCreate(t_Receptora,"t_Receptora",1024,0,1,0);

	vTaskStartScheduler();

	while(1);
}


